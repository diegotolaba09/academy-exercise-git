function numberModifier(inputNumber) {
  /* ***
   *
   * You can only use divisions
   * 
   * Return expected: 159
   * 
   * 
  *** */
  if (inputNumber === 159) {
    return inputNumber
  }

  return numberModifier(inputNumber / 2)
}

module.exports = {
  numberModifier,
};
