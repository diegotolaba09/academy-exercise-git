function stringModifier(inputString) {
  /* ***
   * 
   * Return expected: tSrOiDcBaA
   * 
  *** */
  const arrReverse = inputString.split('').sort().reverse()
  const result = arrReverse.map((value, index) => {
    if (index % 2 === 0) {
      return value.toLowerCase()
    }
    return value
  });
  return result.join('')
}

module.exports = {
  stringModifier,
};
