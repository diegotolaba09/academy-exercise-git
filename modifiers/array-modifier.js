function arrayModifier(inputArray) {
    /* ***
     *
     * Return expected: ['paml', 'rouf', 'moor', 'lalh']
     * 
     *** */
    const arrayMod = inputArray.map(data => {
        const arrData = data.split('');
        const result = arrData.map((data, index) => {
            if (index === 0) {
                return arrData[arrData.length - 1];
            }
            if (arrData.length - 1 === index) {
                return arrData[0];
            }
            return data
        })
        return result
    })
    return arrayMod
}

module.exports = {
    arrayModifier
}