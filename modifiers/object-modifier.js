function objectModifier(inputObject) {
  /* ***
   * 
   * Return expected:
   * 
   * {
   *  eman: "name: John",
   *  emaNtsal: "lastName: Doe",
   *  enohp: 5678,
   *  delbanEsi: false
   * }
   * 
  *** */
  const propertyKey = Object.keys(inputObject);
  const propertyValue = Object.values(inputObject);

  const objUser = {};

  propertyKey.forEach((prop, index) => {
    if (typeof inputObject[prop] === 'boolean') {
      return objUser[formatProp(prop)] = !Boolean(propertyValue[index])
    }

    if (typeof inputObject[prop] === 'number') {
      const arrNumber = inputObject[prop].toString().split('').map(value => Number(value) + 4)
      return objUser[formatProp(prop)] = Number(arrNumber.join(''))
    }

    return objUser[formatProp(prop)] = `${prop}: ${propertyValue[index]}`
  })

  return objUser
}

const formatProp = (prop) => {
  return prop.split("").reverse().join("")
}

module.exports = {
  objectModifier,
};
