const {arrayModifier} = require('./modifiers/array-modifier')
const {numberModifier} = require('./modifiers/number-modifier')
const {objectModifier} = require('./modifiers/object-modifier')
const {stringModifier} = require('./modifiers/string-modifier')

const arrayData = require('./data/array')
const numberData = require('./data/number')
const objectData = require('./data/object')
const stringData = require('./data/string')

console.info('The array result is: ', arrayModifier(arrayData))
console.info('The number result is: ', numberModifier(numberData))
console.info('The object result is: ', objectModifier(objectData))
console.info('The string result is: ', stringModifier(stringData))
